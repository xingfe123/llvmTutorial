#ifndef LEXER_H_
#define LEXER_H_
#include <string>
namespace lexer{
    enum Token{
        Tok_eof = -1,
        Tok_def = -2,
        Tok_extern = -3,
        Tok_identifier = -4,
        Tok_number = -5,
    };
    const std::string Token2String(int token);
    const std::string IdentifierStr();
    double      NumVal();
    

    
    //get toke from standard input
    //return enum Token;
    int NextToken();
    
}

#endif // LEXER_H_
