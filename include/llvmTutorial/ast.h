#pragma once 
#include <map>
namespace AST{
    void NextToken() ;
    extern int CurrentToken;
    void HandleDefinition();
    void HandleExtern();
    void HandleTopLevelExpression();
}



