#include<string>
#include <vector>
#include<memory>
namespace dataStruct{
    // Ternary Search Tree
    template<typename T>
    struct TST{
        explicit TST():_root(nullptr), _size(0){
            _aux_ret = T();
        }
        ~TST(){
            if(_root != nullptr) clear(std::move(_root));
        }
        // to clean the Content of the trie.
        // does the same as the destructor;
        void Clear(){
            if(_root != nullptr) clear(std::move(_root));
            _size = 0;
            _root = nullptr;
        }
        // if there is no the key in the TST return default value for
        // the value return
        const T& Find(const std::string& key){
            node_ptr node(find(_root, key, 0));
            if(node!=nullptr){
                _aux_ret = node->value;
                node.release();
                return _aux_ret;
            }else{
                return _def;
            }
        }
        // 
        void Insert(const std::string& key, const T& value) {
            
        }
        size_t Size() { return _size;}
        //
        void Erase(const std::string& key);
        bool Contains(const std::string& key);
        std::vector<std::string> Keys(const std::string& prefix = "");
        std::string LongestCommonPath();
    private:
        struct node_t{
            T _value;
            char _c;
            std::unique_ptr<node_t> _left, _right, _middle;
            explicit node_t(char ch, T v = T())
                :_value(v), _left(nullptr),_right(nullptr),_middle(nullptr){}
        };
        using node_ptr = std::unique_ptr<node_t>;
        // using vec_ptr  = std::unique_ptr<std::vector<std::string> > >;
        node_ptr _root;
        size_t _size;
        const T _def = T();
        T _aux_ret;
        node_ptr find(node_ptr& n, const std::string& key, size_t d){
            if(n == nullptr){
                return nullptr;
            }else if(key.size()-1 ==d){
                if(n->_c == key[d] && n->value != _def){
                    return node_ptr(n.get());
                }
                return nullptr;
            }else if(n->_c > key[d]){
                return find(n->left, key, d);
            }else if(n->_c == key[d]){
                return find(n->middle, key, d+1);
            }else if(n->_c < key[d]){
                return find(n->right, key, d);
            }
            
        }
        node_ptr insert(node_ptr n, const std::string& key,
                        const T& value, size_t d, bool & created){
            assert(n != nullptr);
            if(key.size()-1 ==d && n->_c == key[d]) {
                if(n->_value == _def) created = true;
                n->_value = value;
            }else if(n->_c == key[d]){
                if(n->middle == nullptr)
                    n->middle = node_ptr(new node_t(key[d+1]));
                n->middle = insert(std::move(n->middle), key, value, d+1, created);
            }else {
                node_ptr& y = n->left;
                if(n->_c < key[d]) y = n->right;
                if(y == nullptr) y = node_ptr(new node_t(key[d]));
                y = insert(std::move(y), key, value, d, created);
            }
            return n;
        }
        void clear(node_ptr n){
            if(n==nullptr) return;
            clear(std::move(n->_left));
            clear(std::move(n->_right));
            clear(std::move(n->_middle));
            n.reset();
        }
        bool erase(node_ptr&n, const std::string& key, size_t d, bool& decrease){
            if(key.size()-1 == d && n!=nullptr){
                
            }
            return false;// todo
            
        }
        std::string longestCommonPath(node_ptr& n, std::string s){
            if(n!=nullptr && n->left == nullptr && n->right == nullptr)
                return longestCommonPath(n->middle,s+n->_c);
        }
        std::string lcp_clean_beforre(node_ptr& n){
            // todo
            return std::string();
        }
        
    };
}
