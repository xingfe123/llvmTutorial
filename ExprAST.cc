#include <gmock/gmock.h>
using namespace std;
namespace {
    struct Value;

    
    struct  ExprAST{
        virtual ~ExprAST(){}
        virtual Value *Codegen() = 0;
    };
    struct NumberExprAST: public ExprAST{
        double _val;
        NumberExprAST(double val):_val(val){}
        // 
        //Value *Codegen(){
        ///    
        // }
        MOCK_METHOD0(Codegen, Value*());
    };

    struct  VariableExprAST:public ExprAST{
        string _name;
        VariableExprAST(const string& name) : _name(name){}
        MOCK_METHOD0(Codegen, Value*());
    };
    struct BinaryExprAST: public ExprAST{
        BinaryExprAST(char op, const ExprAST* lhs, const ExprAST* rhs){}
        MOCK_METHOD0(Codegen, Value*());
    };
    struct CallExprAST :public ExprAST{
        string _callee;
        vector<ExprAST*> _args;
        CallExprAST(const string& callee, vector<ExprAST*>& args)
            :_callee(callee), _args(args){}
        MOCK_METHOD0(Codegen, Value*());
    };

}
