#include "llvmTutorial/lexer.h"
#include <gtest/gtest.h>
// echo "def extern x8 23 4.3"|./test/lexer
TEST(lexer, kk) {
  int tok = lexer::NextToken();
  EXPECT_EQ(tok, lexer::Tok_def);

  tok = lexer::NextToken();
  EXPECT_EQ(tok, lexer::Tok_extern);

  tok = lexer::NextToken();
  EXPECT_EQ(tok, lexer::Tok_identifier);

  tok = lexer::NextToken();
  EXPECT_EQ(tok, lexer::Tok_number);

  close(STDIN_FILENO);
  tok = lexer::NextToken();
  EXPECT_EQ(tok, lexer::Tok_eof);
}

// int main(int argc, char **argv) {
//   ::testing::InitGoogleTest(&argc, argv);
//   return RUN_ALL_TESTS();
// }
