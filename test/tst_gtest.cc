#include "dataStruct/TST.hpp"
#include <gtest/gtest.h>

struct TSTTest : public testing::Test {
  virtual void TearDown() { t.Clear(); }
  dataStruct::TST<int> t;
};
