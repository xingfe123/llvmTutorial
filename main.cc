
#include "llvmTutorial/lexer.h"
#include "llvmTutorial/ast.h"
#include <iostream>
using namespace std;
int main(int argc, char** argv) {
    cerr<<"ready>";
    AST::NextToken();
    while (true) {
        cerr<<"ready>";
        switch (AST::CurrentToken) {
        case lexer::Tok_eof:
            return 0;
        case ';':
            AST::NextToken();
            break;
        case lexer::Tok_def:
            AST::HandleDefinition();
            break;
        case lexer::Tok_extern:
            AST::HandleExtern();
            break;
        default:
            AST::HandleTopLevelExpression();
            break;
        }
    }
}
