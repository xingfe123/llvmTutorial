#include "ast_impl.h"

namespace AST {
using namespace std;
using namespace llvm;
static map<char, int> binOpPrecedence;
static bool binOpPrecedenceIsInit = false;
static void initOpPrecedence(){
    binOpPrecedence['<'] = 10;
    binOpPrecedence['+'] = 20;
    binOpPrecedence['-'] = 20;
    binOpPrecedence['*'] = 30;
    binOpPrecedence['/'] = 40;
    binOpPrecedenceIsInit = true;
}
// Code Generation
// 
// LLVM construct that contains all of the functions and global variables in a
// chunk of code
static unique_ptr<Module> theModule;

//  generate LLVM intructions, keep tack of the current place to insert
//  instruction and has methods to created new instructions
static LLVMContext theContext;
static IRBuilder<> Builder(theContext);
static unique_ptr<legacy::FunctionPassManager> theFPM;
struct ScopeJIT;
struct PrototypeAST;
static unique_ptr<ScopeJIT> theJIT;
static map<string, unique_ptr<PrototypeAST> > functionProtos;

//  keeps tacks of which values are defined in the current scope and what their
//  LLVM representtation is.
static map<string, Value*> nameValues;
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

//
int CurrentToken;

int NextToken(){
    if(!binOpPrecedenceIsInit) initOpPrecedence();
    return CurrentToken = lexer::NextToken();
}


// exprAST base class for all expression nodes;
struct ExprAST{
    virtual ~ExprAST() {}
    virtual Value *Codegen() = 0;
};

struct NumberExprAST: public ExprAST{
    NumberExprAST(const double& val) :_val(val){}
    virtual Value* Codegen(){
        return ConstantFP::get(theContext, APFloat(_val));
    }
    double _val;
};

// expression class for referencing a variable, like "a"
Value* ErrorV(const char* );
struct VariableExprAST: public ExprAST{
    string _name;
    VariableExprAST(const std::string& name):_name(name){}
    Value* Codegen() {
        Value* V = nameValues[_name];
        return V? V: ErrorV("Unknow variable name");
    }

};

struct BinaryExprAST: public ExprAST{
    char _op;
    ExprAST* _lhs, * _rhs;
    BinaryExprAST(char op, ExprAST* lhs, ExprAST* rhs)
            :_op(op), _lhs(lhs), _rhs(rhs){}
    Value* Codegen(){ // just get a value 
        Value* l = _lhs->Codegen();
        Value* r = _rhs->Codegen();
        if(l == 0 || r == 0) {
            return 0;
        }

        switch(_op){
        case '+': return Builder.CreateFAdd(l, r, "addtmp");
        case '-': return Builder.CreateFSub(l, r, "subtmp");
        case '*': return Builder.CreateFMul(l, r, "multmp");
        case '<':
            l = Builder.CreateFCmpULT(l, r, "cmptmp");
            
            return Builder.CreateUIToFP(l, Type::getDoubleTy(theContext),
                                        "bootmp");
        default: return ErrorV("invalid binary operator");
        }
    }
};

struct CallExprAST: public ExprAST{
    string _callee;
    vector<ExprAST*> _args;
    CallExprAST(const string& callee, vector<ExprAST*>& args)
            :_callee(callee), _args(args){}
    Value* Codegen() {
        Function* calleF = theModule->getFunction(_callee);
        if(calleF == 0) return ErrorV("Unknow function refernced");
        if(calleF->arg_size() != _args.size())
            return ErrorV("Incorrect # arguments passed");
        vector<Value*> args;
        for(size_t i = 0, e = _args.size(); i!=e; i++){
            args.push_back(_args[i]->Codegen());
            if(args.back() ==0) return 0;
        }
        return Builder.CreateCall(calleF, args, "calltmp");
    }
};


// which represent the prototype for a function
Function* ErrorFunction(const char*);
struct PrototypeAST{
    string _name;
    vector<string> _args;
    PrototypeAST(const string& s, const vector<string>& v)
            :_name(s), _args(v){}
    Function* Codegen(){
        vector<Type*> Doubles(_args.size(),
                              Type::getDoubleTy(theContext));
        FunctionType* ft =
                FunctionType::get(Type::getDoubleTy(theContext),
                                  Doubles, false);
        Function* F = Function::Create(ft,
                                       Function::ExternalLinkage, _name,
                                       theModule.get());
        // if F conficted, there was already something named 'Name'. 
        
        if(F->getName() != _name){
            F->eraseFromParent();
            F = theModule->getFunction(_name);
        }
        // If it has a body, don't allow redefinition or extern
        
        if(!F->empty()){
            return ErrorFunction("redefinition of function");
        }

        // if F took a differenct number of args, reject .
        if(F->arg_size() != _args.size()) {
            return ErrorFunction("redefintion of function with different # args");
        }
        // set names for all arguments
        size_t idx = 0;
        for(Function::arg_iterator ai = F->arg_begin();
            idx != _args.size(); ++ai, ++idx){
            ai->setName(_args[idx]);
            nameValues[_args[idx]] = &(*ai);
        }
        return F;
    }
};
struct FunctionAST {
    PrototypeAST* _proto;
    ExprAST* _body;
    FunctionAST(PrototypeAST *proto, ExprAST *body)
            :_proto(proto), _body(body){}
    Function* Codegen(){
        nameValues.clear();
        Function* function = _proto->Codegen();
        if(function ==0) return 0;
        BasicBlock* basicBlock = BasicBlock::Create(theContext,
                                                    "entry",
                                                    function);
        Builder.SetInsertPoint(basicBlock);
        if(Value *retVal = _body->Codegen()){
            Builder.CreateRet(retVal);
            verifyFunction(*function);
            return function;
        }
        
        // error reading body, remove function
        function->eraseFromParent();
        return 0;
    }
    
};

/////////////////////////////////////////////////////////////////////////

ExprAST *Error(const char *str) {
  fprintf(stderr, "Error: %s\n", str);
  return 0;
}
PrototypeAST *ErrorP(const char *str) {
  Error(str);
  return 0;
}
FunctionAST* ErrorF(const char* str){
    Error(str);
    return 0;
}
Function* ErrorFunction(const char* str){
    Error(str);
    return 0;
}
// used to report errors found during code generation 
Value* ErrorV(const char* str) {
    Error(str);
    return 0;
}

ExprAST *parseExpression();

ExprAST *parseNumberExpr() {
    
    ExprAST *res = new NumberExprAST(lexer::NumVal());
    NextToken();
    return res;
}
// parenExpr ::= '(' expression ')'
ExprAST *parseParenExpr() {
    NextToken(); // eat '('
    ExprAST *v = parseExpression();
    if (v == 0){
      assert(false);
      return 0;
    }
    if (CurrentToken != ')')
        return Error("Expected )");
    NextToken(); // eat ')'
    return v;
}
// identiferExpr
//     ::= identifer
//     ::= identifer '(' expression* ')' 
ExprAST *parseIdentifierExpr() {
    string idName(lexer::IdentifierStr());
    NextToken();
    if (CurrentToken != '(') // simple variable ref 
        return new VariableExprAST(idName);
    vector<ExprAST *> args;
    NextToken(); // eat (
    while (CurrentToken != ')') {
        ExprAST *arg = parseExpression();
        if (!arg)
            return 0;
        args.push_back(arg);
        if (CurrentToken == ')')
            break;
        if (CurrentToken != ',')
            return Error("Expected ')' or ',' in argment list");
        NextToken();
    }
    NextToken(); // eat the ')'
    return new CallExprAST(idName, args);
}
// primary
//   := indentifierExpr
//   := numberExpr
//   := parenExpr
ExprAST *parsePrimary() {
    // lexer::printToken(CurrentToken());
    // cout<<"parsePrimary"<<lexer::Token2String(CurrentToken)<<endl;
    switch (CurrentToken) {
    case lexer::Tok_identifier:
        return parseIdentifierExpr();
    case lexer::Tok_number:
        return parseNumberExpr();
    case '(':
        return parseParenExpr();
    default:
        cout<<"token"<<CurrentToken
            <<endl;
        return Error("unknow token when expecting an expression");
  }
}


int tokPrecedence(){
    if(!isascii(CurrentToken)) return -1;
    int tokPrec = binOpPrecedence[CurrentToken];
    if(tokPrec <= 0) return -1;
    return tokPrec;
}

// bin-oprhs
// :=(+ primary)*
ExprAST *parseBinOpRHS(const int& exprPrec, ExprAST *LHS) {
  while (true) {
      int tokPrec = tokPrecedence();
      if(tokPrec  < exprPrec) return LHS;
  }
}
// expression
// := primary binoprhs
ExprAST *parseExpression() {
  ExprAST *lhs = parsePrimary();
  assert(lhs != 0);
  return parseBinOpRHS(0, lhs);
}
// prototype
//  ::= id '(' id* ')'
PrototypeAST* parsePrototype(){
    if(CurrentToken != lexer::Tok_identifier){
        return ErrorP("Expected function name in prototype");
    }
    string fnName(lexer::IdentifierStr());
    
    NextToken(); //eat (
    cerr<<fnName<< CurrentToken<<lexer::IdentifierStr()<< endl;
    if(CurrentToken != '(') return ErrorP("Expected '(' in prototype");
        
    vector<string> args;
    while(NextToken() == lexer::Tok_identifier) {
        args.push_back(lexer::IdentifierStr());
    }
    if(CurrentToken != ')') return ErrorP("Expected ')' in prototype");
    NextToken(); //eat )
    return new PrototypeAST(fnName, args);
}
// definition ::= 'def' prototype expression
FunctionAST* parseDefinition(){
    assert(CurrentToken == lexer::Tok_def);
    NextToken(); // eat 'def'
    PrototypeAST* proto = parsePrototype();
    if(proto == 0) {
        assert(false);
        return 0;
    }
    ExprAST* expr = parseExpression();
    if(expr == 0) {
        assert(false);
        return 0;
    }
    return new FunctionAST(proto, expr);
}
// external ::= 'extern' prototype
PrototypeAST* parseExtern() {
    assert(CurrentToken == lexer::Tok_extern);
    NextToken(); // eat 'extern'
    return parsePrototype();
}
// toplevelexpr::= expression
FunctionAST* parseTopLevelExpr() {
    if(ExprAST* e = parseExpression()){
        // make an anonymous proto
        PrototypeAST* proto = new PrototypeAST("", vector<string>());
        return new FunctionAST(proto, e);
    }
    assert(false);
    return 0;
}
/////////////////////////////////////////////////////

void HandleTopLevelExpression(){
    if(parseTopLevelExpr()){
        std::cout<<"Parsed a top level expr"<< std::endl;
    }else{
        NextToken();
    }
}


void HandleDefinition(){
    if(parseDefinition()){
        std::cout<<"Parsed def expr"<< std::endl;
    }else{
        NextToken();
    }
}
void HandleExtern() {
    if(parseExtern()){
        std::cout << "Parse handle extern" << std::endl;
    }else{
        NextToken();
    }
}

void InitializedModuleAndPassManger() {
    theModule = llvm::make_unique<Module>("cool jit", theContext);
    theModule->setDataLayout(theJIT->getTargetMachine().createDataLayout());
    theFPM = llvm::make_unique<FunctionPassManager>(theModule.get());
    theFPM->add(createBasicAliasAnalysisPass());
    theFPM->add(createInstructionCombiningPass());
    theFPM->add(createReassociatePass());
    theFPM->add(createGVNPass());
    theFPM->add(createCFGSimplificationPass());
    theFPM->doInitialization();
}

} // namespace AST
////////////////////////////////////////////////////////////////////////

