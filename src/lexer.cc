#include "llvmTutorial/lexer.h"

#include<string>
#include<vector>
#include<cstdlib>
#include<map>
#include<iostream>
#include<string>
namespace lexer{
using namespace std;
static string identifierStr;
static double numVal;

const string Token2String(int token){
    switch(token){
    case Tok_number:
        return "number";
    case Tok_extern:
        return "extern";
    case Tok_def:
        return "def";
    case Tok_identifier:
        return "identifier";
    default:
        string res;
        res += token;
        return res;
    }
}
const string IdentifierStr() {
    return identifierStr;
}
double NumVal(){
    return numVal;
}

int NextToken() {
    static int lastChar = ' ';
    while(isspace(lastChar)) lastChar = getchar();

    if(isalpha(lastChar)){ // identifer [a-zA-Z]
        identifierStr= lastChar;
        while(isalnum(lastChar = getchar())){
            identifierStr += lastChar;
        }
        if(identifierStr == "def") return Tok_def;
        if(identifierStr == "extern") return Tok_extern;
        return Tok_identifier;
    } else if( isdigit(lastChar) || lastChar == '.'){
        string numStr;
        do{
            numStr += lastChar;
            lastChar = getchar();
        }while(isdigit(lastChar) || lastChar == '.');
        numVal = strtod(numStr.c_str(), 0);
        return Tok_number;
    }
    if(lastChar == '#'){
        do lastChar = getchar();
        while( lastChar!=EOF && lastChar != '\n'
               &&lastChar!='\r');
        if(lastChar != EOF) return NextToken();
    }
    if(lastChar == EOF) {
        return Tok_eof;
    }else{
        int thisChar = lastChar;
        lastChar = getchar();
        return thisChar;
    }

}
}





